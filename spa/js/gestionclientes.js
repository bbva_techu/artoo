var clientesObtenidos;

function getClientes() {

    var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Customers";
    var request = new XMLHttpRequest();

    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            console.log(request.responseText);
            clientesObtenidos = request.responseText;
            procesarClientes();
        }
    };

    request.open("GET", url, true);
    request.send();

}

//var flagUrl = "https://upload.wikimedia.org/wikipedia/en/thumb/b/ba/Flag_of_{country}.svg/800px-Flag_of_{country}.svg.png";
//var flagUrl = "https://en.wikipedia.org/wiki/File:Flag_of_{country}.svg";
var flagUrl = "https://www.countries-ofthe-world.com/flags-normal/flag-of-{country}.png";

function procesarClientes() {

    var JSONClientes = JSON.parse(clientesObtenidos);
    var tabla = document.getElementById("tablaClientes");
    var body = document.createElement("tbody");
    tabla.appendChild(body);

    for (var i = 0; i < JSONClientes.value.length; i++) {
        
        var nuevaFila = document.createElement("tr");

        var columnaNombre = document.createElement("td");
        columnaNombre.innerText = JSONClientes.value[i].ContactName;
        nuevaFila.appendChild(columnaNombre);

        var columnaDireccion = document.createElement("td");
        columnaDireccion.innerText = JSONClientes.value[i].Address;
        nuevaFila.appendChild(columnaDireccion);

        var columnaCiudad = document.createElement("td");
        columnaCiudad.innerText = JSONClientes.value[i].City;
        nuevaFila.appendChild(columnaCiudad);

        var columnaBandera = document.createElement("td");
        var bandera = document.createElement("img");
        var url;
        if (JSONClientes.value[i].Country == "UK") {
            url = flagUrl.replace("{country}", "United-Kingdom");
        } else {
            url = flagUrl.replace("{country}", JSONClientes.value[i].Country);
        }
        //bandera.classList.add("col-lg-4");
        bandera.classList.add("flag");
        bandera.setAttribute("src", url); 
        //bandera.setAttribute("width", "100px");
        columnaBandera.appendChild(bandera);
        //columnaBandera.innerText = JSONClientes.value[i].Country;
        nuevaFila.appendChild(columnaBandera);

        body.appendChild(nuevaFila);

        console.log(JSONClientes.value[i].Country);

    }

}
