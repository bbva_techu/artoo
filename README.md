# Tech U! Final Project - Super Secure Bank
Author: Martin Obiols (e048795)
Date: January 2018

![alt text](https://martinole.bitbucket.io/techu/img/landing.png "Super Secure Bank")


## Brief

Final project for BBVA Tech University 2017 (startup + practitioner bootcamps). Small banking interface with login, account management, last movements and wire transfers. 

Stack: Polymer 2.0-based Web Components, NodeJS, PostgreSQL, MongoDB, Docker

## Changelog - commit Tags

v1.3

* bugfixes and performance improvements

v1.2

* Added autologin feature that uses webcrypto api to implement FIDO-like authentication
* Implemented a security state model that prompts the user for a different second authentication factor depending on the first one that was used for login (password or autologin)

v1.1

* Added Two*factor auth with Google Authenticator for wire transfers
* Added wire transfer between existing users to their Checking accounts

v1.0

* MVP: login, logout, account listing, movement listing for accounts, creating movements

## Functionalities

* Account Registration: Username, Name, Surname, Password and TOTP enrollment using Google Authenticator
* User login: users can provide two types of credentials:
 * the password they chose during account creation
 * optionally, they can use a passwordless login presenting the signature of a challenge that the server will present. This process requires that the user registers a Public Key in JWK format during a normal password-based login.
* Session management: after a successful login, users obtain a JWT with a 10 minute expiration time (no refresh)
* Account management: users can check the balance of two default accounts: Checking and Savings
* Movements: users can check the latest movements for each account: recipient, amount and message
* Transfers: users can make money transfers to other accounts (existing or not), specifying the username of the recipient, amount and a message.
* Security State Model: Users can access basic functionalities using only a single credential (password or challenge-based autologin). All functionalities are considered basic except for money transfers which prompt the user for a second factor of authentication. Depending on the login method, this second factor will be:
 * One-time password from Google Authenticator (something you have): for users presenting a JWT which was obtained by providing a correct password (something you know)
 * Password (something you know): for users presenting a JWT which was obtained by signing a challenge with a browser private key (something you have)

## Architecture

Project consists on 3 main containers:

* NodeJS: dockerized on port 3000:3000
  * Serves Polymer front static files
  * Single Page App with Polymer Components
  * Uses events and Session Storage as communication channel between components
  * Databus Mixin to share common code
  * Provides Banking API
* PostgreSQL: dockerized on port 4000:5432
  * User Table 
```sql
CREATE TABLE users(id SERIAL PRIMARY KEY, username VARCHAR(40) not null, name VARCHAR(40), surname VARCHAR(40), password VARCHAR(100) not null, stsecret VARCHAR(100), browserjwk VARCHAR(300), challenge VARCHAR(40));
```
* MongoDB: dockerized on port 27017

## Security Features

* Application Secrets read from environment variables (not hardcoded)
* Login protected against enumeration and side-channel attacks
* Passwords stored using Bcrypt hashing algorithm with configurable work factor
* Checks to prevent transferring money beyond available account balance
* Checks to prevent users from accesing or modifying info for other users (obviously!)
* SQL Queries are parametrized to prevent SQL injection attacks
* Minor validations performed to illustrate the need of server-side input validation
* FIDO autologin using ECDSA keypairs managed by webcryptoAPI, persisted on a IndexedDB-based keystore and used to sign challenges on a FIDO-like protocol
* Several TODO comments on the code pointing out outstanding security issues that should be properly resolved

## Development Process
around 90 commits
```
$> git shortlog -sn
    86  Martin
     2  Martin Obiols
     1  Martín O
```

Followed Gitflow

```
$> git branch -v
  2fa             99bcdd7 fixed money transferred not appearing on recipients account
  accounts        9a79652 added some bash scripts
  artoo-databus   67d6e08 added working databus
  artoo-home      1431f80 working skeleton for login and movements
  artoo-login     479e144 autofocus on first form field
  artoo-movements 6b413e4 added list and create movement apis + control de canal
  artoo-register  140d574 modified signals for login
  artoo-transfer  40d7cd2 added transfer input validation
  bcrypt          75908ea added example code to hash passwords with bcrypt algorithm
* develop         4a20a76 added v1.2 features to readme
  env-secrets     f01aba8 added prompt to input non-default JWT secret and passed through ENV to nodejs
  logging         cc4b2ef refactoring, commenting and some log traces added
  login-spinner   36d10f8 added spinner, background image and styling
  master          4a20a76 added v1.2 features to readme
  mongodb         0374b2e housekeeping and server side validation
  postgresql      9435ba1 added initial user to dockerfile
  raml            b736ee5 fix
  signalling      c739dfb added session lifecycle signalling and dummy transfer section
  webcryptoapi    c8dd5a4 working now a state model asking for different second auth factor depending on login method
```

Example flow:

```
*   7b7c064 - (hace 2 semanas) Merge branch 'develop' into webcryptoapi - Martin
|\  
| * cc4b2ef - (hace 2 semanas) refactoring, commenting and some log traces added - Martin (bitbucket/logging, logging)
| *   d0e3f35 - (hace 2 semanas) Merge branch 'master' into develop - Martin
| |\  
| | * cb90820 - (hace 2 semanas) modified changelog - Martin (tag: v1.1)
| * | ddb7c6f - (hace 2 semanas) README edited - Martin
| |/  
| * 6dd3f04 - (hace 2 semanas) added demo for 1.1 - Martin
| * 99bcdd7 - (hace 2 semanas) fixed money transferred not appearing on recipients account - Martin (bitbucket/2fa, 2fa)
| *   eec16e4 - (hace 2 semanas) Merge branch 'develop' into 2fa - Martin
| |\  
| | * f5fefa2 - (hace 2 semanas) attempt to actually transfer money - Martin
| * | 7836b07 - (hace 2 semanas) minor edits - Martin
| |/  
| * ea50fef - (hace 2 semanas) added 2fa using speakeasy - Martin
```

## Usage Guide

1. Clean existing images: cleanimages.sh
2. Build docker images: build.sh
3. Launch containers: start.sh
4. Go to http://localhost:3000
5. Register a new account and login



