#!/bin/bash
# delete dangling volumes
sudo docker volume rm $(sudo docker volume ls -f dangling=true -q)
