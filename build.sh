#!/bin/bash
echo "### Building images..."
npm install
cd spa
bower install
polymer build --sources css/* js/* --extra-dependencies img/*
#cp -R bower_components build/default/
cd ..
sudo docker build -t olemoudi/nodejs .
cd mongodb
sudo docker build -t olemoudi/mongodb .
cd ..
cd postgresql
sudo docker build -t olemoudi/postgresql .
cd ..


