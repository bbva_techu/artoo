/*jshint esversion: 6 */
var banner = `
  #####                                  #####                                        ######                       
 #     # #    # #####  ###### #####     #     # ######  ####  #    # #####  ######    #     #   ##   #    # #    # 
 #       #    # #    # #      #    #    #       #      #    # #    # #    # #         #     #  #  #  ##   # #   #  
  #####  #    # #    # #####  #    #     #####  #####  #      #    # #    # #####     ######  #    # # #  # ####   
       # #    # #####  #      #####           # #      #      #    # #####  #         #     # ###### #  # # #  #   
 #     # #    # #      #      #   #     #     # #      #    # #    # #   #  #         #     # #    # #   ## #   #  
  #####   ####  #      ###### #    #     #####  ######  ####   ####  #    # ######    ######  #    # #    # #    # 
Author: Martin Obiols
`;
console.log(banner);
var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

//////////////////////////
/* Requires and Clients */
//////////////////////////
var bcrypt = require('bcrypt');
var twofa = require('speakeasy');
var qr = require('qrcode');
var requestJson = require('request-json');
var path = require('path');
var bodyParser = require('body-parser');
var expressjwt = require('express-jwt');
var jwt = require('jsonwebtoken');
var rand = require('csprng'); // usage rand(bits, radix) where radix is size of alphabet
var pg = require('pg');
var atob = require('atob');
var b64ab = require('base64-arraybuffer');
var WebCrypto = require("node-webcrypto-ossl");
var webcrypto = new WebCrypto({
  directory: "key_storage"
});
var mongo_client = require('mongodb').MongoClient;
//TODO Sec: Force loading of Postgre creds from env
var postgresqlClient = new pg.Client({ user: 'docker', password : 'docker', host: 'postgresql', port: 5432, database: 'artoodb'});
//postgresqlClient.connect();

//////////////////////////
/* Constants and Config */
//////////////////////////
//TODO Sec: Force loading of secret from env, no default fallback
var CSPRNG_RADIX = 10; //size of alphabet
var CSPRNG_ENTROPY = 64; //bits
var JWT_SECRET = process.env.JWT_SECRET || 'supersicrit123'; 
if (JWT_SECRET == 'supersicrit123') {
    console.log("[Security Warning] using default JWT Secret");
}
var JWT_EXPIRATION = '10m';
//TODO Perf: Benchmark hashrate 
var bcrypt_workfactor = 14;
// Example hash is used during non-existent user login flows to prevent timing attacks
var bcrypt_examplehash = '$2a$15$r7y0EhDa6SLGbcCYeuqzgee1spv9ABpecdxPUJkb/nHb.rOyotDi2';
var MONGO_URL = 'mongodb://mongodb:27017/local';


app.listen(port);
// disable 304 responses
app.disable('etag');


// Serve Single-Page App static files
app.use('/', express.static(path.join(__dirname, 'spa')));

//////////////////////////
/*      middlewares     */
//////////////////////////

app.use(bodyParser.json());
// Everything under /api will require a JWT 
app.use('/api', expressjwt({secret: JWT_SECRET, credentialsRequired: true}));

// Expired or absent JWTs will generate UnauthorizedError
app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401).send({"code" : "401", "msg" : "Your session has expired"});
  } else {
    next();
  }
});

// Log all requests
app.use(function(req, res, next) {
    console.log(req.ip + " | " + req.method + " | " + req.path + " -| Username = "+req.user);
    next();
});


/*
 * GET /strequest
 *
 * Generates and returns a valid TOTP to be included in a registration request
 *
 *  code: return code
 *  otpurl: QR code data url suitable for img tag src attributes
 *  otpsecret: TOTP seed
 *  otp (optional) : currently valid OTP (for autofill)
 *
 * Example Return Value:
 * {
 *  code : 200,
 *  otpurl: data:image/gif;base64,R0lGODlhyAA.....,
 *  otpsecret: IFXEAXTXKZVUYLDWIZVVQ62PNFMGQZSHM52VCSJVINTXSWROKJJB,
 *  otp : 453243
 * }
 * */

app.get('/strequest', function(req, res) {

    var secret = twofa.generateSecret();
    var name = encodeURI('Super Secure Bank (TechU!)');
    var otpurl = twofa.otpauthURL({ secret: secret.ascii, label: name, algorithm: 'sha512' });
    qr.toDataURL(otpurl, function(err, data_url) {

       var t = "";

       if (req.query.autofill == "1") {

        t = twofa.totp({
        secret: secret.base32,
        encoding: 'base32'
        });

       }

        resbody = JSON.stringify({"code" : "200", "otpurl" : data_url, "otpsecret" : secret.base32, "otp" : t});
        res.status(200).send(resbody);

    });

});


/*
 * GET /login
 *
 * Authenticates a user via username+password. Returns a JWT valid for JWT_EXPIRATION time
 *
 *  code: return code
 *  token : HMAC-SHA256 signed JSON including id, username, nickname, surname and login method
 *
 * Example Return Value:
 * {
 *  code : 200,
 *  token : <jwt>
 * }
 * */
//TODO codequality: Fix this callback hell using Promises
app.post('/login', function(req, res) {
    console.log('login attempt for user: '+req.body.user);
    userdata = {
                username : req.body.username,
                password : req.body.password
    };
    // retrieve user data from db
    postgresqlClient.connect();
    singleuser_query = "SELECT * from users where username = $1";
    postgresqlClient.query(singleuser_query, [userdata.username], function(err, result){
        // only 1 user should be returned
        if (result.rows.length != 1) {
            console.log("users query returned a total of: "+result.rows.length);
            // hash something anyway to prevent side-channel leaks via timing attacks
            bcrypt.compare(req.body.password, bcrypt_examplehash, function(err, bres) {
                error(res, "Wrong User or Password");
            });
        } else {
            bcrypt.compare(req.body.password, result.rows[0].password, function(err, bres) {
                if (bres == true) {
                    console.log('successful login by '+ result.rows[0].username);
                    tokendata = {
                                "id"       : result.rows[0].id, 
                                "username" : result.rows[0].username,
                                "nickname" : result.rows[0].name,
                                "surname"  : result.rows[0].surname,
                                "login_method" : "password"
                                };
                    token = jwt.sign(tokendata, JWT_SECRET, {expiresIn: JWT_EXPIRATION});
                   
                    // if jwk present on login, save browser for autologin
                    if (req.body.jwk != undefined) {
                        challenge = rand(CSPRNG_ENTROPY, CSPRNG_RADIX);
                        update_query = 'UPDATE users SET challenge = $1, browserjwk = $2 WHERE username = $3';
                        query = postgresqlClient.query(update_query, [challenge, req.body.jwk, userdata.username], function(err, result){
                            if(err) {
                                console.log(err);
                                console.log("unable to update jwk info for user after login");
                            } else {
                                console.log("Successfully updated JWK info for user");
                            }
                        });
                    }

                    resbody = JSON.stringify({"code" : "200", "token" : token});
                    res.status(200).send(resbody);
                } else {
                    error(res, "Wrong User or Password");
                }
            });
        }
    });
});

/* Register new User */
app.post('/register', function(req, response) {
    console.log('new registration: '+ req.body.username);
    userdata = {
                username : req.body.username,
                nickname : req.body.nickname,
                surname : req.body.surname,
                password : req.body.password
    };
    //postgresqlClient.connect();
    singleuser_query = "SELECT * from users where username = $1";
    postgresqlClient.query(singleuser_query, [userdata.username], function(err, result){
        if (result.rows.length != 0) {
            msg = "Usuario ya existe";
            resbody = JSON.stringify({"code" : "ER", "msg" : msg});
            response.status(200).send(resbody);
        } else {
            if (! /^([\w\-]{3,})$/.test(userdata.nickname)) {
                    error(response, "Name must have at least 3 characters");
            } else if (! /^([\w\-]{3,})$/.test(userdata.surname)) {
                    error(response, "Surname must have at least 3 characters");
            } else if (! /^([a-zA-Z0-9\-_]{3,})$/.test(userdata.username)) {
                    error(response, "Username must have at least 3 alphanumeric characters or hyphens");
            } else if (! /^([\w]{2,})$/.test(userdata.password)) {
                    error(response, "Password too short");
            } else if (userdata.password != req.body.password2) {
                    error(response, "Passwords do not match");
            } else {
                //validate OTP
                var tokenValidates = twofa.totp.verify({
                secret: req.body.otpsecret,
                encoding: 'base32',
                token: req.body.otp,
                window: 10
                });
                console.log("otp validation: "+tokenValidates);
                if (!tokenValidates) {
                    resbody = JSON.stringify({"code" : "400", "msg" : "Invalid OTP"});
                    response.status(200).send(resbody);

                } else {

                    insert_query = 'INSERT INTO users (username, name, surname, password, stsecret, challenge) VALUES($1, $2, $3, $4, $5, $6);';
                    hash = bcrypt.hashSync(userdata.password, bcrypt_workfactor);
                    //TODO security: some risk of SQLi in otpsecret parameter exists here. We shouldnt be reaching this code if otpsecret contains common SQLi attack vectors, but best practices call for input validation anyway
                    query = postgresqlClient.query(insert_query, [userdata.username, userdata.nickname, userdata.surname, hash, req.body.otpsecret, '12345'], function(err, result){
                        if(err) {
                            console.log(err);
                            msg = ""+err;
                            //TODO: raw postgresql msg should not be returned to user
                            resbody = JSON.stringify({"code" : "IE", "msg" : msg});
                            response.status(200).send(resbody);
                        } else {
                            msg = "User created";
                            resbody = JSON.stringify({"code" : "200", "msg" : msg});
                            response.status(200).send(resbody);
                        }
                    });
                }
            }

        }
    });
});


/*
 * {
 * username,
 * name,
 * balance
 * }
 * */
app.get('/api/accounts', function(req, res) {
    mongo_client.connect(MONGO_URL, function(err, db) {
        if(err) {
            error(res, "Error retrieving accounts");
            console.log(err);
        } else {
            var askforpassword = false;
            if (req.user.login_method == "browserjwk") {
                askforpassword = true;
            }
            console.log("Successfully connected to mongodb server");
            var accounts = db.collection('accounts');
            accounts.find({"username" : req.user.username}).toArray(function(err, accs) {
                // if no accounts present, create default ones
                if (accs.length == 0) {
                    console.log('No accounts for user, creating default ones');
                    checkingaccount = {
                                username : req.user.username,
                                name : "Checking",
                                balance : 100
                    };
                    savingsaccount = {
                                username : req.user.username,
                                name : "Savings",
                                balance : 1000
                    };
                    accounts.insertOne(checkingaccount);
                    accounts.insertOne(savingsaccount);
                    console.log("inserting default accounts");
                    accounts.find({"username" : req.user.username}).toArray(function(err, accs2) {
                        resbody = JSON.stringify({"code" : "200" , "accounts" : accs2, "login_method" : req.user.login_method});
                        res.status(200).send(resbody);
                        db.close();
                    });
                } else {
                    console.log("Accounts found");
                    resbody = JSON.stringify({"code" : "200" , "accounts" : accs, "login_method" : req.user.login_method});
                    res.status(200).send(resbody);
                    db.close();
                }
            });
        }
    });
});


/* Movements Local Mongo DB */
app.get('/api/movements/:accountname', function(req, res) {
    mongo_client.connect(MONGO_URL, function(err, db) {
        if(err) {
            error(res, "Error retrieving movements");
            console.log(err);
        } else {
            console.log("Successfully connected to mongodb server");
            var movements = db.collection('movements');
            movements.find({"username" : req.user.username, "accountname" : req.params.accountname}).sort({"date": -1}).toArray(function(err, movs) {
                resbody = JSON.stringify({"code" : "200" , "movements" : movs});
                res.status(200).send(resbody);
            });
            db.close();
        }
    });
});

/*
 * {
 * username,
 * accountname,
 * detail,
 * date,
 * amount,
 * recipient
 * }
 * */

app.post('/api/transfer', function(req, res) {
    if (! /^([\w\-]{2,})$/.test(req.body.recipient)) {
            error(res, "Recipient's name must have at least 2 characters");
    } else if (isNaN(req.body.amount) && req.body.amount > 0) { //TODO: this validation is very weak
            error(res, "Amount must be a number");
    } else if (! /^([\w ].*)$/.test(req.body.message)) {
            error(res, "Message must be at least 3 characters long");
    } else {
        transferdata = {
                    username : req.user.username,
                    accountname : req.body.account,
                    recipient : req.body.recipient,
                    amount : 0-req.body.amount,
                    date : new Date(), //TODO: date format?
                    message : req.body.message
        };
        //postgresqlClient.connect();
        singleuser_query = "SELECT * from users where username = $1";
        postgresqlClient.query(singleuser_query, [transferdata.username], function(err, result){

            // only 1 user should be returned
            // 0 is also valid
            if (result.rows.length > 1) {

                error(res, "More than one user with that name");

            } else {
                
                var fa = false;
                console.log("login method: "+req.user.login_method);

                if (req.user.login_method == 'password') {

                    // verify token
                    fa = twofa.totp.verify({
                    secret: result.rows[0].stsecret,
                    encoding: 'base32',
                    token: req.body.otp,
                    window: 10
                    });
                } else {
                    fa = bcrypt.compareSync(req.body.password, result.rows[0].password);
                }

                if (!fa) {
                       if (req.user.login_method == 'password') { 
                            error(res, "Invalid OTP");
                       } else {
                            error(res, "Invalid password");
                       }

                } else {

                    mongo_client.connect(MONGO_URL, function(err, db) {
                        if(err) {

                            error(res, "Error creating movement");
                            console.log(err);

                        } else {

                            console.log("Successfully connected to mongodb server");

                        }
                        var accounts = db.collection('accounts');
                        accounts.find({"username" : req.user.username, "name" : transferdata.accountname}).toArray(function(err, accs) {
                            // if no accounts present, create default ones
                            
                            if (req.body.amount <= accs[0].balance && req.body.amount > 0) {
                                console.log("balance: "+accs[0].balance);
                                tosubstract = 0 - req.body.amount;
                                accounts.updateOne(
                                        {"name" : transferdata.accountname, "username" : transferdata.username },
                                        { $inc: {"balance" : tosubstract}}
                                        );
                                var movements = db.collection('movements');
                                movements.insertOne(transferdata);


                                // transfer money to recipient's Checking account
                                accounts.find({"username" : transferdata.recipient, "name" : "Checking"}).toArray(function(err, accs) {
                                    console.log("accs: "+accs);
                                    if (accs.length > 0) {
                                        toadd = req.body.amount - 0;
                                        accounts.updateOne(
                                                {"name" : "Checking", "username" : transferdata.recipient },
                                                { $inc: {"balance" : toadd}}
                                                );
                                        newtransferdata = {
                                                    username : transferdata.recipient,
                                                    accountname : 'Checking',
                                                    recipient : transferdata.recipient,
                                                    amount : req.body.amount,
                                                    date : new Date(), //TODO: date format?
                                                    message : "Transfer received from "+transferdata.username +": "+transferdata.message
                                        };
                                        console.log("newtransferdata: "+newtransferdata);
                                        movements.insertOne(newtransferdata);

                                    }

                                    resbody = JSON.stringify({"code" : "200", "msg" : "Transfer Done"});
                                    res.status(200).send(resbody);
                                    db.close();
                                    
                                });

                            } else {
                                error(res, "Your account has unsufficient funds");
                                db.close();
                            }
                        });

                    });
                }
            }
        });

    }
});

/* Mock Endpoints for testing */

app.get('/api/mock/checkauth', function(req, res) {

    resbody = JSON.stringify({"code" : "200", "msg" : "authed"});
    res.status(200).send(resbody);

});

app.post('/api/mock/transfer', function(req, res) {

    resbody = JSON.stringify({"code" : "200", "msg" : "Transfer done"});
    res.status(200).send(resbody);

});

app.get('/api/mock/movements', function(req, res) {

    movements = [
        { "username" : req.user.username,
            "detail" : "Recibo de agua",
            "date" : "12/12/2017",
            "amount" : -60.5,
            "recipient" : "Gas Natural"
        },
        { "username" : req.user.username,
            "detail" : "Compra en AmazonES",
            "date" : "11/12/2017",
            "amount" : -20.95,
            "recipient" : "AmazonES"
        },
        { "username" : req.user.username,
            "detail" : "Transferencia recibida",
            "date" : "10/12/2017",
            "amount" : 160.5,
            "recipient" : "lo que te debo"
        },
    ];

    resbody = JSON.stringify({"code" : "200", "movements" : movements});
    res.status(200).send(resbody);

});

// For benchmarking
// siege -b http://localhost:3000/hash?workfactor=X
app.get('/hash', function (req, res) {
    var w = req.query.workfactor || 12;
    bcrypt.hash('1234567890', w - 0, function(err, hash) {
        res.send(hash);
    });

});

function error(response, msg) {
    error(response, msg, "400");
}

function error(response, msg, code) {
    console.log(msg);
    resbody = JSON.stringify({"code" : code, "msg" : msg});
    response.status(200).send(resbody);
}

// test challenges
app.get('/autologin_challenge', function(req, res) {
    challenge = rand(64, 10);
    resbody = JSON.stringify({"code" : "200", "challenge" : challenge});
    res.status(200).send(resbody);
});

app.post('/autologin_challenge', function(req, res) {
    
        //postgresqlClient.connect();
        singleuser_query = "SELECT * from users where browserjwk = $1";
        postgresqlClient.query(singleuser_query, [req.body.jwk], function(err, result){
            console.log(result);

            if (result == undefined) {
                error(res, "Sorry, browser not found", 401);
            }
            // only 1 user should be returned
            else if (result.rows.length == 0) {

                error(res, "Sorry, that browser is not recognized", 401);

            }
            else if (result.rows.length > 1) {

                error(res, "More than onse user returned for that jwk");

            } else {
                console.log("1 user found");

                challenge = rand(CSPRNG_ENTROPY, CSPRNG_RADIX);
                console.log("challenge is: "+challenge);
                update_query = 'UPDATE users SET challenge = $1 WHERE browserjwk = $2';
                query = postgresqlClient.query(update_query, [challenge, req.body.jwk], function(err, result){
                    if(err) {
                        console.log(err);
                        msg = ""+err;
                        //TODO security: raw postgresql msg should not be returned to user
                        resbody = JSON.stringify({"code" : "500", "msg" : msg});
                        res.status(200).send(resbody);
                    } else {
                        msg = "Challenge updated";
                        resbody = JSON.stringify({"code" : "200", "challenge" : challenge});
                        res.status(200).send(resbody);
                    }
                });
            }
        });
});


app.post('/autologin', function(req, res) {
    // IMPORTANT: all errors for this autologin should be generic and without side-channel leaks, so this function can not act as an Oracle for crypto attacks
    console.log('autologin attempt for user: '+req.body.user);
    //postgresqlClient.connect();
    singleuser_query = "SELECT * from users where browserjwk = $1";
    postgresqlClient.query(singleuser_query, [req.body.jwk], function(err, result){

        // 
        if (result.rows.length != 1) {

            error(res, "Autologin error", 401);

        } else {

            var challenge = str2ab(result.rows[0].challenge);
            var jwk = JSON.parse(atob(result.rows[0].browserjwk));
            var signature = b64ab.decode(req.body.signature);

            webcrypto.subtle.importKey(
                "jwk", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
                jwk,
                {   //these are the algorithm options
                    name: "ECDSA",
                    namedCurve: "P-256", //can be "P-256", "P-384", or "P-521"
                },
                true, //whether the key is extractable (i.e. can be used in exportKey)
                ["verify"] //"verify" for public key import, "sign" for private key imports
            )
            .then(function(publicKey){
                //returns a publicKey (or privateKey if you are importing a private key)
                console.log("Succesfully imported Kpub");

                webcrypto.subtle.verify(
                    {
                        name: "ECDSA",
                        hash: {name: "SHA-256"}, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
                    },
                    publicKey, //from generateKey or importKey above
                    signature, //ArrayBuffer of the signature
                    challenge //ArrayBuffer of the data
                )
                .then(function(isvalid){
                    //returns a boolean on whether the signature is true or not
                    console.log("Signature verification result: "+isvalid);

                    if (isvalid) {
                        tokendata = {
                                    "id"       : result.rows[0].id, 
                                    "username" : result.rows[0].username,
                                    "nickname" : result.rows[0].name,
                                    "surname"  : result.rows[0].surname,
                                    "login_method" : "browserjwk"
                                    };
                        token = jwt.sign(tokendata, JWT_SECRET, {expiresIn: JWT_EXPIRATION});
                        resbody = JSON.stringify({"code" : "200", "token" : token});
                        res.status(200).send(resbody);
                        //Clear or change current challenge to prevent signature reuse
                        challenge = rand(CSPRNG_ENTROPY, CSPRNG_RADIX);
                        console.log("challenge is: "+challenge);
                        update_query = 'UPDATE users SET challenge = $1 WHERE browserjwk = $2';
                        query = postgresqlClient.query(update_query, [challenge, req.body.jwk], function(err, result){
                            if(err) {
                                console.log(err);
                                console.log("Security Warning, could not change current challenge after autologin. A valid signature could be replayed");
                            } else {
                                console.log("Challenge updated");

                            }
                        });
                    } else {
                        error(res, "Autologin error", 401);
                    }
                })
                .catch(function(err){
                    console.log("Signature verification failed with error");
                    console.error(err);
                    error(res, "Autologin error", 401);
                });

            })
            .catch(function(err){
                console.log("Key import operation failed with error");
                console.error(err);
                error(res, "Autologin error", 401);
            });
        }
    });

});

app.get('/crypto', function (req, res) {

    var jwk = JSON.parse(atob(req.query.jwk));
    var signature = b64ab.decode(req.query.signature);
    var challenge = str2ab(req.query.challenge);
    webcrypto.subtle.importKey(
        "jwk", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
        jwk,
        {   //these are the algorithm options
            name: "ECDSA",
            namedCurve: "P-256", //can be "P-256", "P-384", or "P-521"
        },
        true, //whether the key is extractable (i.e. can be used in exportKey)
        ["verify"] //"verify" for public key import, "sign" for private key imports
    )
    .then(function(publicKey){
        //returns a publicKey (or privateKey if you are importing a private key)
        console.log(publicKey);

        webcrypto.subtle.verify(
            {
                name: "ECDSA",
                hash: {name: "SHA-256"}, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
            },
            publicKey, //from generateKey or importKey above
            signature, //ArrayBuffer of the signature
            challenge //ArrayBuffer of the data
        )
        .then(function(isvalid){
            //returns a boolean on whether the signature is true or not
            console.log("is valid? -> "+isvalid);
        })
        .catch(function(err){
            console.error(err);
        });




    })
    .catch(function(err){
        console.error(err);
    });
});

function str2ab(str) {
  var buf = new ArrayBuffer(str.length*2); // 2 bytes for each char
  var bufView = new Uint16Array(buf);
  for (var i=0, strLen=str.length; i<strLen; i++) {
    bufView[i] = str.charCodeAt(i);
  }
  return buf;
}
function ab2str(buf) {
  return String.fromCharCode.apply(null, new Uint16Array(buf));
}
