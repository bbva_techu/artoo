# imagen base
FROM node:latest

# directorio de trabajo
WORKDIR /app

# copiamos archivos al nivel del Dockerfile
ADD spa/build/default /app/spa
#ADD spa/img /app/spa/img
#ADD spa/bower.json /app
#RUN bower install
#CMD ["cp", "-R", "/app/bower_components/", "/app/spa/bower_components"]
ADD package.json /app
ADD server.js /app

# Dependencias
RUN npm install

# Abrir puerto
EXPOSE 3000

# comando
CMD  ["npm", "start"]





