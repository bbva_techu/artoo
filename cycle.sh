#!/bin/bash
#sudo docker stop $(sudo docker ps -a -q)
#sudo docker rm $(sudo docker ps -a -q)



echo "killing instances..."
sudo docker kill mongodb
sudo docker kill nodejs
sudo docker kill postgresql
echo "Removing stopped instances..."
sudo docker rm mongodb
sudo docker rm nodejs
sudo docker rm postgresql
# delete dangling volumes
#sudo docker volume rm $(sudo docker volume ls -f dangling=true -q)
#!/bin/bash
echo "### Building images..."
npm install
cd spa
bower install
polymer build --sources css/* js/* --extra-dependencies img/*
#cp -R bower_components build/default/
cd ..
sudo docker build -t olemoudi/nodejs .
cd mongodb
sudo docker build -t olemoudi/mongodb .
cd ..
cd postgresql
sudo docker build -t olemoudi/postgresql .
cd ..


#!/bin/bash
echo "### Launching containers..."
echo "Creating network..."
sudo docker network create redtechu
echo "Running mongodb..."
sudo docker run -v /opt/mongodata:/data/db -p 27017:27017 --name mongodb -d  --net redtechu olemoudi/mongodb
echo "Running nodejs..."
echo -n "Input JWT Secret: "
read -s jwtsecret
echo
sudo docker run -p 3000:3000 --name nodejs -d --net redtechu -e JWT_SECRET="$jwtsecret" olemoudi/nodejs
echo "Running postgresql..."
sudo docker run -p 4000:5432  --name postgresql -d --net redtechu olemoudi/postgresql
echo "finished"
sudo docker ps

echo "Finished"
