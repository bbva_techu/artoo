// ECDSA primitives
var keypairname ="mykeypair";
var keypair, keystore;

document.addEventListener("DOMContentLoaded", function() {
    "use strict";

    if (!window.crypto || !window.crypto.subtle) {
        alert.log("Your current browser does not support the Web Cryptography API! This page will not work.");
        return;
    }

    if (!window.indexedDB) {
        alert("Your current browser does not support IndexedDB. This page will not work.");
        return;
    }

    keystore = new KeyStore();
    keystore.open().
    then(function() {
        console.log("Successfully created local keystore backed by IndexedDB");
    }).
    catch(function(err) {
        alert("Could not open key store: " + err.message);
    });

});

    function generateKey() {
        window.crypto.subtle.generateKey(
        {
            name: "ECDSA",
            namedCurve: "P-256", //can be "P-256", "P-384", or "P-521"
        },
            false, //whether the key is extractable (i.e. can be used in exportKey)
            ["sign", "verify"] //can be any combination of "sign" and "verify"
        )
        .then(function(key){
            //returns a keypair object
            keypair = key;
            console.log(key.publicKey);

            window.crypto.subtle.exportKey(
                "jwk", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
                keypair.publicKey //can be a publicKey or privateKey, as long as extractable was true
            )

                .then(function(keydata){
                    //returns the exported key data
                    console.log(keydata);
                    return keydata;
                })
                .catch(function(err){
                    console.error(err);
                });

            return keystore.saveKey(keypair.publicKey, keypair.privateKey, keypairname);

            })
            .catch(function(err){
                console.error(err);
            });
    }

    function loadKey() {
        keystore.getKey("name", keypairname)
            .then(function(pair) {
                keypair = pair;
                console.log(keypair.publicKey);
                window.crypto.subtle.exportKey(
                    "jwk", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
                    keypair.publicKey //can be a publicKey or privateKey, as long as extractable was true
                )
                    .then(function(keydata){
                        //returns the exported key data
                        //alert(JSON.stringify(keydata));
                        console.log(keydata);
                        return keydata;
                    })
                    .catch(function(err){
                        console.error(err);
                    });

            }).catch(function(err) {console.log("error");});
        
    }

    function sign(data) {
        var data_arr = str2ab(data);
        window.crypto.subtle.sign(
            {
                name: "ECDSA",
                hash: {name: "SHA-256"}, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
            },
            keypair.privateKey, //from generateKey or importKey above
            data_arr //ArrayBuffer of data you want to sign
        )
        .then(function(signature){
            //returns an ArrayBuffer containing the signature
            sig = new Uint16Array(signature);
            console.log(ab2str(signature).toString('hex'));
            //console.log(signature.toString().hexEncode());
            return sig; 
        })
        .catch(function(err){
            console.error(err);
        });
    }

//http://qnimate.com/digital-signature-using-web-cryptography-api/

function str2ab(str) {
  var buf = new ArrayBuffer(str.length*2); // 2 bytes for each char
  var bufView = new Uint16Array(buf);
  for (var i=0, strLen=str.length; i<strLen; i++) {
    bufView[i] = str.charCodeAt(i);
  }
  return buf;
}
function ab2str(buf) {
  return String.fromCharCode.apply(null, new Uint16Array(buf));
}
function toHexString(byteArray) {
  return Array.from(byteArray, function(byte) {
    return ('0' + (byte & 0xFF).toString(16)).slice(-2);
  }).join('');
}
String.prototype.hexEncode = function(){
    var hex, i;

    var result = "";
    for (i=0; i<this.length; i++) {
        hex = this.charCodeAt(i).toString(16);
        //result += ("000"+hex).slice(-4);
        result += hex;
    }

    return result;
};

/*
function importKey() {
window.crypto.subtle.importKey(
    "jwk", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
    {   //this is an example jwk key, other key types are Uint8Array objects
        kty: "EC",
        crv: "P-256",
        x: "zCQ5BPHPCLZYgdpo1n-x_90P2Ij52d53YVwTh3ZdiMo",
        y: "pDfQTUx0-OiZc5ZuKMcA7v2Q7ZPKsQwzB58bft0JTko",
        ext: true,
    },
    
    {   //these are the algorithm options
        name: "ECDSA",
        namedCurve: "P-256", //can be "P-256", "P-384", or "P-521"
    },
    false, //whether the key is extractable (i.e. can be used in exportKey)
    ["verify"] //"verify" for public key import, "sign" for private key imports
)
.then(function(publicKey){
    //returns a publicKey (or privateKey if you are importing a private key)
    console.log(publicKey);
})
.catch(function(err){
    console.error(err);
});    
}
*/

/*
function exportKey() {
    window.crypto.subtle.exportKey(
        "jwk", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
        keypair.publicKey //can be a publicKey or privateKey, as long as extractable was true
    )
    .then(function(keydata){
        //returns the exported key data
        //alert(JSON.stringify(keydata));
        console.log(keydata);
        return keydata;
    })
    .catch(function(err){
        console.error(err);
    });
}

function sign(data) {
    var data_arr = str2ab(data);
    window.crypto.subtle.sign(
        {
            name: "ECDSA",
            hash: {name: "SHA-256"}, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
        },
        keypair.privateKey, //from generateKey or importKey above
        data_arr //ArrayBuffer of data you want to sign
    )
    .then(function(signature){
        //returns an ArrayBuffer containing the signature
        //sig = new UintArray(signature);
        console.log(signature);
        return signature; 
    })
    .catch(function(err){
        console.error(err);
    });
}

function saveKey() {
    keyStore.saveKey(keyPair.publicKey, keyPair.privateKey, name);
        }).
    window.localStorage.setItem("rememberme_key", JSON.stringify(exportKey()));
    console.log("Key saved");
}

function loadKey() {
    keypair = JSON.parse(window.localStorage.getItem("rememberme_key"));
    keyStore.getKey(keyPair.publicKey, keyPair.privateKey, name);
    if (keypair) {
        console.log("Successfully loaded key from localstorage");
    } else {
        console.log("no key found");
    }
}

function str2ab(str) {
  var buf = new ArrayBuffer(str.length*2); // 2 bytes for each char
  var bufView = new Uint16Array(buf);
  for (var i=0, strLen=str.length; i<strLen; i++) {
    bufView[i] = str.charCodeAt(i);
  }
  return buf;
}
function ab2str(buf) {
  return String.fromCharCode.apply(null, new Uint16Array(buf));
}


/* 
 * Key Persistence
 */

// Creates and saves key pairs. Requires keystore.js to be loaded first.

/*
document.addEventListener("DOMContentLoaded", function() {
    "use strict";

    if (!window.crypto || !window.crypto.subtle) {
        alert("Your current browser does not support the Web Cryptography API! This page will not work.");
        return;
    }

    if (!window.indexedDB) {
        alert("Your current browser does not support IndexedDB. This page will not work.");
        return;
    }

    var keyStore = new KeyStore();
    keyStore.open().
    then(function() {
        document.getElementById("create-key").addEventListener("click", handleCreateKeyPairClick);
        populateKeyListing(keyStore);
    }).
    catch(function(err) {
        alert("Could not open key store: " + err.message);
    });


    function populateKeyListing(keyStore) {
        keyStore.listKeys().
        then(function(list) {
            for (var i=0; i<list.length; i++) {
                addToKeyList(list[i].value);
            }
        }).
        catch(function(err) {
            alert("Could not list keys: " + err.message);
        });
    }


    function addToKeyList(savedObject) {
        var dataUrl = createDataUrlFromByteArray(new Uint8Array(savedObject.spki));
        var name = escapeHTML(savedObject.name);

        document.getElementById("list-keys").insertAdjacentHTML(
            'beforeEnd',
            '<li><a download="' + name + '.publicKey" href="' + dataUrl + '">' + name + '</a></li>');
    }



    // Key pair creation section
    function handleCreateKeyPairClick() {
        var algorithmName, usages;

        var name = document.getElementById("created-key-name").value;
        if (!name) {
            alert("Must specify a name for the new key.");
            return;
        }

        // Depending on whether it is a signing key or an encrypting
        // key, different algorithmNames and usages are needed to
        // generate the new key pair.
        var selection = document.getElementsByName("created-key-type");
        if (selection[0].checked) { // Signing key
            algorithmName = "RSASSA-PKCS1-v1_5";
            usages = ["sign", "verify"];
        } else if (selection[1].checked) { // Encrypting key
            algorithmName = "RSA-OAEP";
            usages = ["encrypt", "decrypt"];
        } else {
            alert("Must select kind of key first.");
            return;
        }

        window.crypto.subtle.generateKey(
            {
                name: algorithmName,
                modulusLength: 2048,
                publicExponent: new Uint8Array([1, 0, 1]),  // 24 bit representation of 65537
                hash: {name: "SHA-256"}
            },
            false,  // Cannot extract new key
            usages
        ).
        then(function(keyPair) {
            return keyStore.saveKey(keyPair.publicKey, keyPair.privateKey, name);
        }).
        then(addToKeyList).
        catch(function(err) {
            alert("Could not create and save new key pair: " + err.message);
        });
    }



    // Utility functions

    function escapeHTML(s) {
        return s.toString().replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&apos;");
    }

    function createDataUrlFromByteArray(byteArray) {
        var binaryString = '';
        for (var i=0; i<byteArray.byteLength; i++) {
            binaryString += String.fromCharCode(byteArray[i]);
        }
        return "data:application/octet-stream;base64," + btoa(binaryString);
    }
});

*/
