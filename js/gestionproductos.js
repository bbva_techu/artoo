var productosObtenidos;

function getProductos() {

    var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Products";
    var request = new XMLHttpRequest();

    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            console.log(request.responseText);
            productosObtenidos = request.responseText;
            procesarProductos();
        }
    };

    request.open("GET", url, true);
    request.send();

}


function procesarProductos() {

    var JSONProductos = JSON.parse(productosObtenidos);
    var tabla = document.getElementById("tablaProductos");
    var body = document.createElement("tbody");
    tabla.appendChild(body);

    for (var i = 0; i < JSONProductos.value.length; i++) {
        
        var nuevaFila = document.createElement("tr");

        var columnaNombre = document.createElement("td");
        columnaNombre.innerText = JSONProductos.value[i].ProductName;

        var columnaPrecio = document.createElement("td");
        columnaPrecio.innerText = JSONProductos.value[i].UnitPrice;

        var columnaStock = document.createElement("td");
        columnaStock.innerText = JSONProductos.value[i].UnitsInStock;

        nuevaFila.appendChild(columnaNombre);
        nuevaFila.appendChild(columnaPrecio);
        nuevaFila.appendChild(columnaStock);

        body.appendChild(nuevaFila);


        console.log(JSONProductos.value[i].ProductName);
    }

}
