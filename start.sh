#!/bin/bash
echo "### Launching containers..."
echo "Creating network..."
sudo docker network create redtechu
echo "Running mongodb..."
sudo docker run -v /opt/mongodata:/data/db -p 27017:27017 --name mongodb -d  --net redtechu olemoudi/mongodb
echo "Running nodejs..."
echo -n "Input JWT Secret: "
read -s jwtsecret
echo
sudo docker run -p 3000:3000 --name nodejs -d --net redtechu -e JWT_SECRET="$jwtsecret" olemoudi/nodejs
echo "Running postgresql..."
sudo docker run -p 4000:5432  --name postgresql -d --net redtechu olemoudi/postgresql
echo "finished"
sudo docker ps
echo "Finished"

