#!/bin/bash
#sudo docker stop $(sudo docker ps -a -q)
#sudo docker rm $(sudo docker ps -a -q)

echo "killing instances..."
sudo docker kill mongodb
sudo docker kill nodejs
sudo docker kill postgresql
echo "Removing stopped instances..."
sudo docker rm mongodb
sudo docker rm nodejs
sudo docker rm postgresql
# delete dangling volumes
#sudo docker volume rm $(sudo docker volume ls -f dangling=true -q)
